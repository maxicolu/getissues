//
//  Issue.swift
//  ShowIssuesProyect
//
//  Created by User on 5/18/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class IssueObject: AnyObject
{
    let idIssue:NSNumber
    let subject:String
    let issueDescription:String?
    let startDate:Date?
    let priority:String?
    let stimatedHours:Int?
    let spendHours:Int?
    let author:String?
    
    init(idIssue:NSNumber,subject:String,description:String?,startDate:Date?,author:String?,priority:String?,stimatedHours:Int?,spendHours:Int?)
    {
        self.idIssue = idIssue
        self.subject = subject
        self.issueDescription = description
        self.startDate = startDate
        self.author = author
        self.priority = priority
        self.stimatedHours = stimatedHours
        self.spendHours = spendHours
    }
    
    
}
