//
//  DetailIssueTableViewController.swift
//  ShowIssuesProyect
//
//  Created by User on 5/20/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class DetailIssueTableViewController: UITableViewController {

    var issue:IssueObject?
    
    func configNavigationItem()
    {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.setImage(UIImage(named: "backImage"), for: .highlighted)
        backButton.setTitle("Back", for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        backButton.backgroundColor = UIColor.clear
        backButton.frame = CGRect(x: 0, y: 0, width: 60, height: 40)
        let backButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backButtonItem
        self.navigationItem.title = "REDMINE MINI"
        
    }
    
    func backAction(sender:AnyObject)
    {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func configTableView()
    {
        tableView.separatorStyle = .none
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        tableView.estimatedSectionHeaderHeight = 52
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.configNavigationItem()
        self.configTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sectionView = UIView()
        sectionView.backgroundColor = UIColor.white
        
        let issuesLabel = UILabel()
        issuesLabel.backgroundColor = UIColor.customGrayColor()
        issuesLabel.textColor = UIColor.white
        issuesLabel.text = "DETAIL"
        issuesLabel.textAlignment = .center
        issuesLabel.font = UIFont.customLightTextFont(size: 18.0)
        
        sectionView.addSubview(issuesLabel)
        issuesLabel.translatesAutoresizingMaskIntoConstraints = false
        issuesLabel.leadingAnchor.constraint(equalTo: sectionView.leadingAnchor, constant: 0.0).isActive = true
        issuesLabel.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10.0).isActive = true
        issuesLabel.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor, constant: 0.0).isActive = true
        issuesLabel.widthAnchor.constraint(equalToConstant: 107).isActive = true
        issuesLabel.heightAnchor.constraint(equalToConstant: 42).isActive = true
        
        let authorNameLabel = UILabel()
        authorNameLabel.text = issue?.author
        authorNameLabel.font = UIFont.customBoldTextFont(size: 20.0)
        authorNameLabel.textAlignment = .right
        authorNameLabel.textColor = UIColor.customGrayColor()
        sectionView.addSubview(authorNameLabel)
        authorNameLabel.translatesAutoresizingMaskIntoConstraints = false
        authorNameLabel.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 5).isActive = true
        authorNameLabel.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor, constant: 0).isActive = true
        authorNameLabel.trailingAnchor.constraint(equalTo: sectionView.trailingAnchor, constant: -10).isActive = true
        
        return sectionView
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailInfoIdentifier", for: indexPath) as! DetailInfoTableViewCell

        // Configure the cell...
        if let issue = issue
        {
            let subjectText = "Subject: "
            let attrStringSubject = NSMutableAttributedString(string: subjectText + issue.subject)
            let condensedLightFont = UIFont.customLightTextFont(size: 14.0)
            let condensedBoldFont = UIFont.customBoldTextFont(size: 14.0)
            attrStringSubject.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: subjectText.characters.count))
            attrStringSubject.addAttributes([NSFontAttributeName : condensedLightFont], range: NSRange(location: subjectText.characters.count,length: attrStringSubject.string.characters.count - subjectText.characters.count))
            attrStringSubject.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringSubject.string.characters.count))
            cell.detailLabel_1.attributedText = attrStringSubject
            
            if let issueDescription = issue.issueDescription
            {
                let descriptionText = "Description: "
                let attrStringDescription = NSMutableAttributedString(string: descriptionText + issueDescription)
                attrStringDescription.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: descriptionText.characters.count))
                attrStringDescription.addAttributes([NSFontAttributeName : condensedLightFont], range: NSRange(location: descriptionText.characters.count,length: attrStringDescription.string.characters.count - descriptionText.characters.count))
                attrStringDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringDescription.string.characters.count))
                cell.detailLabel_2.attributedText = attrStringDescription
            }
            else
            {
                let descriptionText = "Description: "
                let attrStringDescription = NSMutableAttributedString(string: descriptionText)
                attrStringDescription.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: descriptionText.characters.count))
                attrStringDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringDescription.string.characters.count))
                cell.detailLabel_2.attributedText = attrStringDescription
            }
            
            if let startDate = issue.startDate
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM"
                let month = dateFormatter.string(from:startDate)
                
                dateFormatter.dateFormat = "dd"
                let day = dateFormatter.string(from:startDate)
                
                cell.dateView.detailLabel_1.text = month.uppercased()
                cell.dateView.detailLabel_2.text = day
            }
            else
            {
                cell.dateView.detailLabel_1.text = ""
                cell.dateView.detailLabel_2.text = ""
            }
            
            if let priority = issue.priority?.uppercased()
            {
                let priorityText = "PRIORITY: "
                let attrStringPriority = NSMutableAttributedString(string: priorityText + priority)
                attrStringPriority.addAttributes([NSFontAttributeName : UIFont.customLightTextFont(size: 20.0)], range: NSRange(location: 0,length: priorityText.characters.count))
                attrStringPriority.addAttributes([NSFontAttributeName : UIFont.customBoldTextFont(size: 20.0)], range: NSRange(location: priorityText.characters.count,length: attrStringPriority.string.characters.count - priorityText.characters.count))
                attrStringPriority.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: attrStringPriority.string.characters.count))
                cell.detailLabel_3.attributedText = attrStringPriority
            }
            else
            {
                let priorityText = "PRIORITY: "
                let attrStringPriority = NSMutableAttributedString(string: priorityText)
                attrStringPriority.addAttributes([NSFontAttributeName : UIFont.customLightTextFont(size: 20.0)], range: NSRange(location: 0,length: priorityText.characters.count))
                 attrStringPriority.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: attrStringPriority.string.characters.count))
                cell.detailLabel_3.attributedText = attrStringPriority
            }
            
            if let stimateHours =  issue.stimatedHours
            {
                let stringStimateHours = String(describing:stimateHours)
                cell.countLabel_1.text = stringStimateHours
            }
            if let spendHours = issue.spendHours
            {
                let stringSpendHours = String(describing: spendHours)
                cell.countLabel_2.text = stringSpendHours
            }
        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
