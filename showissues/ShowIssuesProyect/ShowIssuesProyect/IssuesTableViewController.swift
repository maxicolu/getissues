//
//  IssuesTableViewController.swift
//  ShowIssuesProyect
//
//  Created by User on 5/18/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class IssuesTableViewController: BaseTableViewController {

    var issuesToShow:Array<IssueObject>?
    var issueSelected:IssueObject?
    
    func reloadIssues()
    {
        LibraryAPI.sharedInstance.getIssues { (issues:Array<IssueObject>?, error:Error?) in
            
            self.issuesToShow = issues
            
            DispatchQueue.main.async
            {
                    if let error = error
                    {
                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                
                    self.hideLoadingView()
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
            }
        
        }
        
    }
    
    func addRefreshControl()
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(reloadIssues), for: UIControlEvents.valueChanged)
    }
    
    func configNavigationItem()
    {
        self.navigationController?.navigationBar.barTintColor = UIColor.navBarColor()
        self.navigationItem.title = "REDMINE MINI"
    }
    
    func configTableView()
    {
        tableView.separatorStyle = .none
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        tableView.estimatedSectionHeaderHeight = 52
    }
    
    //MARK - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        self.configTableView()
        self.configNavigationItem()
        self.addRefreshControl()
        self.showLoadingView()
        self.reloadIssues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sectionView = UIView()
        sectionView.backgroundColor = UIColor.white
        
        let issuesLabel = UILabel()
        issuesLabel.backgroundColor = UIColor.customGrayColor()
        issuesLabel.textColor = UIColor.white
        issuesLabel.text = "ISSUES"
        issuesLabel.textAlignment = .center
        issuesLabel.font = UIFont.customLightTextFont(size: 18.0)
        
        sectionView.addSubview(issuesLabel)
        issuesLabel.translatesAutoresizingMaskIntoConstraints = false
        issuesLabel.leadingAnchor.constraint(equalTo: sectionView.leadingAnchor, constant: 0.0).isActive = true
        issuesLabel.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10.0).isActive = true
        issuesLabel.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor, constant: 0.0).isActive = true
        issuesLabel.widthAnchor.constraint(equalToConstant: 107).isActive = true
        issuesLabel.heightAnchor.constraint(equalToConstant: 42).isActive = true
        
        return sectionView
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if  issuesToShow != nil && issuesToShow!.count > 0
        {
            self.tableView.backgroundView = nil
            
            return 1
        }
        else
        {
            self.tableView.separatorStyle = .none
            
            let noElementsLabel = UILabel()
            noElementsLabel.backgroundColor = UIColor.white
            noElementsLabel.textColor = UIColor.black
            noElementsLabel.text = "No data is currently available. Please pull down to refresh."
            noElementsLabel.textAlignment = .center
            noElementsLabel.font = UIFont.italicSystemFont(ofSize: 20.0)
            noElementsLabel.numberOfLines = 0
            noElementsLabel.sizeToFit()
            
            self.tableView.backgroundView = noElementsLabel
            
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let issues = issuesToShow
        {
            return issues.count
        }
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableCellIdentifier", for: indexPath) as! InfoTableViewCell
        
        let issue = issuesToShow![indexPath.row]
        
        let subjectText = "Subject: "
        let attrStringSubject = NSMutableAttributedString(string: subjectText + issue.subject)
        let condensedLightFont = UIFont.customLightTextFont(size: 14.0)
        let condensedBoldFont = UIFont.customBoldTextFont(size: 14.0)
        attrStringSubject.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: subjectText.characters.count))
        attrStringSubject.addAttributes([NSFontAttributeName : condensedLightFont], range: NSRange(location: subjectText.characters.count,length: attrStringSubject.string.characters.count - subjectText.characters.count))
        attrStringSubject.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringSubject.string.characters.count))
        cell.detailLabel_1.attributedText = attrStringSubject
        
        if let issueDescription = issue.issueDescription
        {
            let descriptionText = "Description: "
            let attrStringDescription = NSMutableAttributedString(string: descriptionText + issueDescription)
            attrStringDescription.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: descriptionText.characters.count))
            attrStringDescription.addAttributes([NSFontAttributeName : condensedLightFont], range: NSRange(location: descriptionText.characters.count,length: attrStringDescription.string.characters.count - descriptionText.characters.count))
            attrStringDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringDescription.string.characters.count))
            cell.detailLabel_2.attributedText = attrStringDescription
        }
        else
        {
            let descriptionText = "Description: "
            let attrStringDescription = NSMutableAttributedString(string: descriptionText)
            attrStringDescription.addAttributes([NSFontAttributeName : condensedBoldFont], range: NSRange(location: 0,length: descriptionText.characters.count))
            attrStringDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.customGrayColor(), range: NSRange(location: 0, length: attrStringDescription.string.characters.count))
            cell.detailLabel_2.attributedText = attrStringDescription
        }
        
        if let startDate = issue.startDate
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM"
            let month = dateFormatter.string(from:startDate)
            
            dateFormatter.dateFormat = "dd"
            let day = dateFormatter.string(from:startDate)
            
            cell.dateView.detailLabel_1.text = month.uppercased()
            cell.dateView.detailLabel_2.text = day
        }
        else
        {
            cell.dateView.detailLabel_1.text = ""
            cell.dateView.detailLabel_2.text = ""
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        issueSelected = issuesToShow![indexPath.row]
        
        self.showLoadingView()
        LibraryAPI.sharedInstance.getIssue(issueID: issueSelected!.idIssue.stringValue) { (issue:IssueObject?, error:Error?) in
            
            DispatchQueue.main.async
            {
                if let error = error
                {
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
                self.hideLoadingView()
                self.issueSelected = issue
                self.performSegue(withIdentifier: "ShowDettailIssue", sender: nil)
            }
        }
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        (segue.destination as! DetailIssueTableViewController).issue = issueSelected
    }
    

}
