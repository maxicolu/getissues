//
//  IssueDAO.swift
//  ShowIssuesProyect
//
//  Created by User on 5/21/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import CoreData

class IssueDAO: AnyObject
{
    var issues:Array<IssueObject>?
    
    func getIssues() -> Array<IssueObject>
    {
        guard issues == nil else {return issues!}
        
        issues = Array<IssueObject>()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"Issue")
        request.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(request)
            if results.count > 0
            {
                for entity in results as! [NSManagedObject]
                {
                    let isssueID = entity.value(forKey:"idIssue" ) as! NSNumber
                    let subject = entity.value(forKey:"subject" ) as! String
                    let description = entity.value(forKey:"issueDescription" ) as! String
                    let startDate = entity.value(forKey:"startDate" ) as? Date
                    let priority = entity.value(forKey:"priority" ) as? String
                    let stimatedHours = entity.value(forKey:"stimatedHours" ) as? Int
                    let spendHours = entity.value(forKey:"spendHours" ) as? Int
                    let author = entity.value(forKey:"author" ) as? String
                    
                     let issue = IssueObject(idIssue:isssueID,subject: subject, description: description, startDate: startDate,author:author,priority:priority,stimatedHours:stimatedHours,spendHours:spendHours)
                    issues?.append(issue)
                }
            }
        }
        catch
        {
        }
        return issues!
    }
    
    func cleanAllIssues() -> ()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Issue")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch let error as NSError {
            // Handle error
            print(error.localizedDescription)
        }
        
        issues?.removeAll()
    }
    
    func assignIssues(issuesToAppend:Array<IssueObject>) -> ()
    {
        self.cleanAllIssues()
        
        
        issues = Array<IssueObject>()
        issues!.append(contentsOf: issuesToAppend)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        for issue in issues!
        {
            let newIssue = NSEntityDescription.insertNewObject(forEntityName: "Issue", into: context)
            
            newIssue.setValue(issue.idIssue, forKey: "idIssue")
            newIssue.setValue(issue.subject, forKey: "subject")
            
            if let issueDescription = issue.issueDescription
            {
                newIssue.setValue(issueDescription, forKey: "issueDescription")
            }
            
            if let startDate = issue.startDate
            {
                newIssue.setValue(startDate, forKey: "startDate")
            }
            if let priority = issue.priority
            {
                newIssue.setValue(priority, forKey: "priority")
            }
            if let stimatedHours = issue.stimatedHours
            {
                newIssue.setValue(stimatedHours, forKey: "stimatedHours")
            }
            if let spendHours = issue.spendHours
            {
                newIssue.setValue(spendHours, forKey: "spendHours")
            }
            if let author = issue.author
            {
                newIssue.setValue(author, forKey: "author")
            }
        }
        
        do
        {
            try context.save()
        }
        catch
        {
        }
        
    }
    
    func getIssue(issueID:String) -> IssueObject?
    {
        if issues == nil
        {
            _ = self.getIssues()
        }
        
        let indexOfIssue = self.issues!.index { (issue:IssueObject) -> Bool in
            return issue.idIssue.stringValue == issueID
        }
        
        guard let index = indexOfIssue else
        {
            return nil
        }
        
        return issues?[index]
    }
    
    func updateIssue(issue:IssueObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"Issue")
        request.returnsObjectsAsFaults = false
        let predicate = NSPredicate(format: "idIssue=%d", argumentArray: [issue.idIssue.intValue])
        request.predicate = predicate
        
        do
        {
            let results = try context.fetch(request)
            
            if results.count > 0
            {
                let issueToUpdate = results[0] as AnyObject
                
                issueToUpdate.setValue(issue.subject, forKey: "subject")
                
                if let issueDescription = issue.issueDescription
                {
                    issueToUpdate.setValue(issueDescription, forKey: "issueDescription")
                }
                
                if let startDate = issue.startDate
                {
                    issueToUpdate.setValue(startDate, forKey: "startDate")
                }
                if let priority = issue.priority
                {
                    issueToUpdate.setValue(priority, forKey: "priority")
                }
                if let stimatedHours = issue.stimatedHours
                {
                    issueToUpdate.setValue(stimatedHours, forKey: "stimatedHours")
                }
                if let spendHours = issue.spendHours
                {
                    issueToUpdate.setValue(spendHours, forKey: "spendHours")
                }
                if let author = issue.author
                {
                    issueToUpdate.setValue(author, forKey: "author")
                }
                
                do
                {
                    try context.save()
                }
                catch
                {
                }
                
            }
        }
        catch
        {
        }
        
    }
    
}
