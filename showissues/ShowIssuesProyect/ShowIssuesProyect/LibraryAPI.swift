//
//  LibraryAPI.swift
//  ShowRedditProyect
//
//  Created by User on 3/23/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class LibraryAPI: AnyObject
{
    static let sharedInstance = LibraryAPI()
    let internetReachability = Reachability.forInternetConnection()
    private let BaseURLService = "http://demo.redmine.org/"
    private let issuesService = "issues.json"
    
    func getIssues(completion:@escaping ((Array<IssueObject>?,Error?) -> ()) ) -> ()
    {
        let internetStatus = internetReachability?.currentReachabilityStatus()
        
        guard internetStatus != NotReachable else
        {
            completion(IssueDAO().getIssues(),nil)
            return
        }
        
        let httpClient = HTTPClient()
        httpClient.getData(URLService: BaseURLService+issuesService) { (data:Data?, error:Error?) in
            
            guard error == nil else
            {
                completion(IssueDAO().getIssues(),error)
                return
            }
            
            do
            {
                var issues = Array<IssueObject>()
                let JSONObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                if let issuesArray = ((JSONObj as? Dictionary<String,AnyObject>)?["issues"]) as? Array<Dictionary<String,AnyObject>>
                {
                    for issue in issuesArray
                    {
                        let isssueID = issue["id"] as! NSNumber
                        let subject = issue["subject"] as! String
                        let description = issue["description"] as! String
                        let dateString = issue["created_on"]
                        
                        var startDate:Date?
                        if let dateString = dateString
                        {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                            startDate = dateFormatter.date(from: dateString as! String)
                        }
                        
                        let issue = IssueObject(idIssue:isssueID,subject: subject, description: description, startDate: startDate,author:nil,priority:nil,stimatedHours:nil,spendHours:nil)
                        issues.append(issue)
                    }
                }
                
                let issuesDAO = IssueDAO()
                issuesDAO.assignIssues(issuesToAppend: issues)
                
                completion(issues,error)
                
            }
            catch
            {
                completion(nil,error)
                return

            }
            
        }
    }
    
    func getIssue(issueID:String,completion:@escaping ((IssueObject?,Error?) -> ()) ) -> ()
    {
        let internetStatus = internetReachability?.currentReachabilityStatus()
        
        guard internetStatus != NotReachable else
        {
            let issueRecovered = IssueDAO().getIssue(issueID: issueID)
            completion(issueRecovered ,nil)
            return
        }
        
        let URLRequest = BaseURLService + "issues/" + issueID + ".json"
        let httpClient = HTTPClient()
        httpClient.getData(URLService: URLRequest,completion: { (data:Data?, error:Error?) in
            
            guard error == nil else
            {
                let issueRecovered = IssueDAO().getIssue(issueID: issueID)
                completion(issueRecovered,error)
                return
            }
            
            do
            {
                let JSONObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                if let issueDict = ((JSONObj as? Dictionary<String,AnyObject>)?["issue"]) as? Dictionary<String,AnyObject>
                {
                    let isssueID = issueDict["id"] as! NSNumber
                    let subject = issueDict["subject"] as! String
                    let description = issueDict["description"] as! String
                    let dateString = issueDict["created_on"]
                    let priority = (issueDict["priority"] as? Dictionary<String,AnyObject>)?["name"] as? String
                    let stimatedHours = issueDict["done_ratio"] as? Int
                    let spendHours = issueDict["spent_hours"] as? Int
                    let author = (issueDict["author"] as? Dictionary<String,AnyObject>)?["name"] as? String
                    
                    var startDate:Date?
                    if let dateString = dateString
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                        startDate = dateFormatter.date(from: dateString as! String)
                    }
                    
                    let issue = IssueObject(idIssue:isssueID,subject: subject, description: description, startDate: startDate,author:author,priority:priority,stimatedHours:stimatedHours,spendHours:spendHours)
                    
                    let issueDAO = IssueDAO()
                    issueDAO.updateIssue(issue:issue)
                    
                    completion(issue,error)
                }
            }
            catch
            {
                completion(nil,error)
            }
        })
        
    }
    
}
