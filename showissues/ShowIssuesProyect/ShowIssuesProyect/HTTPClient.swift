//
//  HTTPClient.swift
//  ShowRedditProyect
//
//  Created by User on 3/23/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class HTTPClient: AnyObject
{
    func getData(URLService:String,completion:@escaping (Data?,Error?) -> ()) -> ()
    {
        let request = URLRequest(url:URL(string: URLService)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            
            if (data != nil)
            {
                completion(data,error)
            }
            else
            {
                completion(nil,error)
            }
        }
        
        task.resume()
    }
}
