//
//  ExtensionFont.swift
//  ShowIssuesProyect
//
//  Created by User on 5/19/17.
//  Copyright © 2017 Max. All rights reserved.
//

import Foundation
import UIKit

extension UIFont
{
    class func customLightTextFont(size:CGFloat) -> UIFont
    {
        return UIFont(name: "OpenSans-CondensedLight", size: size)!
    }
    
    class func customBoldTextFont(size:CGFloat) -> UIFont
    {
        return UIFont(name: "OpenSans-CondensedBold", size: size)!
    }
}
