//
//  InfoTableViewCell.swift
//  ShowIssuesProyect
//
//  Created by User on 5/18/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet weak var dateView: infoCircleView!
    @IBOutlet weak var detailLabel_2: UILabel!
    @IBOutlet weak var detailLabel_1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
