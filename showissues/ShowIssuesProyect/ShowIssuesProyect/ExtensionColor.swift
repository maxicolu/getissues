//
//  ExtensionColor.swift
//  ShowIssuesProyect
//
//  Created by User on 5/19/17.
//  Copyright © 2017 Max. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    class func customGrayColor () -> UIColor
    {
        return UIColor(red:94/255.0, green:94/255.0, blue:94/255.0, alpha:1.0)
    }
    
    class func navBarColor () -> UIColor
    {
        return UIColor(red: 63/255.0, green: 255/255.0, blue: 177/255.0, alpha: 1.0)
    }
    
    class func grayTextColor() -> UIColor
    {
        return UIColor(red: 163/255.0, green: 163/255.0, blue: 163/255.0, alpha: 1.0)
    }
}
