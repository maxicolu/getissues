//
//  infoCircleView.swift
//  ShowIssuesProyect
//
//  Created by User on 5/19/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class infoCircleView: UIView
{

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    let detailLabel_1 = UILabel()
    let detailLabel_2 = UILabel()
    
    override func awakeFromNib()
    {
        self.backgroundColor = UIColor.clear
        
        let lineView = UIView()
        lineView.backgroundColor = UIColor.customGrayColor()
        self.addSubview(lineView)
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.widthAnchor.constraint(equalToConstant: 9.0).isActive = true
        lineView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        lineView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        lineView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        let circleView = UIView()
        circleView.backgroundColor = UIColor.customGrayColor()
        self.addSubview(circleView)
        circleView.layer.cornerRadius = self.frame.size.width/2.0
        circleView.layer.masksToBounds = true
        circleView.translatesAutoresizingMaskIntoConstraints = false
        circleView.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        circleView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.0).isActive = true
        circleView.heightAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        circleView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        detailLabel_1.textColor = UIColor.white
        detailLabel_1.backgroundColor = UIColor.clear
        detailLabel_1.numberOfLines = 0
        detailLabel_1.textAlignment = .center
        detailLabel_1.font = UIFont.customLightTextFont(size: 19.0)
        self.addSubview(detailLabel_1)
        detailLabel_1.translatesAutoresizingMaskIntoConstraints = false
        detailLabel_1.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        detailLabel_1.topAnchor.constraint(equalTo: self.topAnchor, constant: 15.0).isActive = true
        //detailLabel_1.heightAnchor.constraint(equalToConstant: self.frame.size.width/2.0).isActive = true
        detailLabel_1.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        detailLabel_2.textColor = UIColor.white
        detailLabel_2.backgroundColor = UIColor.clear
        detailLabel_2.numberOfLines = 0
        detailLabel_2.textAlignment = .center
        detailLabel_2.font = UIFont.customBoldTextFont(size: 24.0)
        self.addSubview(detailLabel_2)
        detailLabel_2.translatesAutoresizingMaskIntoConstraints = false
        detailLabel_2.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        detailLabel_2.topAnchor.constraint(equalTo: detailLabel_1.bottomAnchor, constant: -6.0).isActive = true
        detailLabel_2.heightAnchor.constraint(equalToConstant: self.frame.size.width/2.0).isActive = true
        detailLabel_2.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
    }
    
}
