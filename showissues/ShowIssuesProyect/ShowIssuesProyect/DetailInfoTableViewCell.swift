//
//  DetailInfoTableViewCell.swift
//  ShowIssuesProyect
//
//  Created by User on 5/20/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class DetailInfoTableViewCell: UITableViewCell {

    
    @IBOutlet weak var countLabel_2: UILabel!
    @IBOutlet weak var countLabel_1: UILabel!
    @IBOutlet weak var detailLabel_3: UILabel!
    @IBOutlet weak var detailLabel_2: UILabel!
    @IBOutlet weak var detailLabel_1: UILabel!
    @IBOutlet weak var dateView: infoCircleView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        self.countLabel_1.layer.cornerRadius = self.countLabel_1.frame.size.width/2.0
        self.countLabel_1.layer.masksToBounds = true
        
        self.countLabel_2.layer.cornerRadius = self.countLabel_2.frame.size.width/2.0
        self.countLabel_2.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
